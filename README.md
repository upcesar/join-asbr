#Venha trabalhar na ActualSales!

##A Empresa

A ActualSales é uma empresa de marketing digital focada em geração de leads através de Landing Pages.
Relaxa, nós também não sabíamos o que isso significa ;)

##Vagas

###Frontend Developer

####Requisitos:

- Experiência com HTML, CSS e Javascript.
- Conhecimento forte em Bootstrap 3. 
- Replicar o design de páginas com grande fidelidade. (Photoshop)
- Conhecimento em algum framework javascript, como por exemplo: jQuery.
- Vivência com Desenvolvimento Web em geral.
- Ser capaz de desenvolver websites responsivos para desktop, tablets ou celulares.
- Saber usar o Git.
- Ter pelo menos um pouco de experiência com alguma linguagem como PHP, Ruby, Python, Java ou C#.


####Bônus:
- Conhecimento em AngularJS ( Ou pelo menos muita vontade de aprender :] )
- Gulp || Grunt
- Browserify || Webpack

####Nosso stack atual:
- PHP (framework baseado no Zend)
- MySQL
- Git (Bitbucket com deploy contínuo)
- Jquery
- Bootstrap
- Apache
- Varnish
- **Importante ressaltar que esse stack está sempre aberto para novas tecnologias**

####Oferecemos:
- Horário Flexível
- Ambiente informal (chinelo e breja no trabalho é OK)
- (VR || VA) && VT
- Plano de saúde e dental

####Como me candidatar?
Não queremos ver o seu CV, mas sim o seu código.
Para isso, elaboramos um pequeno teste de aptidões que pode ser realizado no seu tempo.
As instruções para execução desse teste estão localizadas na pasta teste-frontend desse mesmo repositório.

####Dúvidas
Para dúvidas ou mais informações, fique à vontade para nos contactar através do email <vagas-ti@actualsales.com.br>.


Obrigado e boa sorte!